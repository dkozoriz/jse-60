package ru.t1.dkozoriz.tm.api.service;

public interface IServiceLocator {

    ILoggerService getLoggerService();

    IPropertyService getPropertyService();

    ITokenService getTokenService();

}