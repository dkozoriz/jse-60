package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskUpdateByIndexRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskUpdateByIndexListener extends AbstractTaskListener {

    public TaskUpdateByIndexListener() {
        super("task-update-by-index", "update task by index.");
    }

    @Override
    @EventListener(condition = "@taskUpdateByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[UPDATE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = TerminalUtil.nextLine();
        taskEndpoint.taskUpdateByIndex(new TaskUpdateByIndexRequest(getToken(), index, name, description));
    }

}