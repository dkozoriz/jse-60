package ru.t1.dkozoriz.tm.listener;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.api.listener.IListener;
import ru.t1.dkozoriz.tm.api.model.ICommand;
import ru.t1.dkozoriz.tm.api.service.ITokenService;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements IListener {

    @NotNull
    @Getter
    protected final String name;

    @Nullable
    protected final String description;

    @Nullable
    @Getter
    protected final String argument;

    @Autowired
    protected ITokenService tokenService;

    protected String getToken() {
        return tokenService.getToken();
    }

    protected void setToken(@Nullable final String token) {
        tokenService.setToken(token);
    }

    protected AbstractListener(@NotNull String name, @Nullable String description) {
        this.name = name;
        this.description = description;
        argument = null;
    }

    protected AbstractListener(@NotNull String name, @Nullable String description, @Nullable String argument) {
        this.name = name;
        this.description = description;
        this.argument = argument;
    }

    public abstract void handler(@NotNull ConsoleEvent consoleEvent);

    @NotNull
    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String result = "";
        final boolean hasName = !name.isEmpty();
        final boolean hasArgument = argument != null && !argument.isEmpty();
        final boolean hasDescription = description != null && !description.isEmpty();
        if (hasName) result += name;
        if (hasArgument) result += hasName ? ", " + argument : argument;
        if (hasDescription) result += ": " + description;
        return result;
    }

}