package ru.t1.dkozoriz.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.dkozoriz.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.dkozoriz.tm.event.ConsoleEvent;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

@Component
public final class TaskRemoveByIndexListener extends AbstractTaskListener {

    public TaskRemoveByIndexListener() {
        super("task-remove-by-index", "remove task by index.");
    }

    @Override
    @EventListener(condition = "@taskRemoveByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull ConsoleEvent consoleEvent) {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        taskEndpoint.taskRemoveByIndex(new TaskRemoveByIndexRequest(getToken(), index));
    }

}