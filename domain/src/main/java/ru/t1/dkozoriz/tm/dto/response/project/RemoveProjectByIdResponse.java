package ru.t1.dkozoriz.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.dkozoriz.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class RemoveProjectByIdResponse extends AbstractResponse {
}
