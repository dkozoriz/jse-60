package ru.t1.dkozoriz.tm.repository.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.IAbstractDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoRepository<T extends AbstractModelDto> implements IAbstractDtoRepository<T> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<T> getClazz();

    @Override
    public void add(@NotNull final T model) {
        entityManager.persist(model);
    }

    @Override
    public void update(@NotNull final T model) {
        entityManager.merge(model);
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    @Nullable
    public List<T> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Override
    @Nullable
    public T findById(@NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.id = :id";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final Integer index) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz())
                .setFirstResult(1)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public long getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult();
    }

    @Override
    public void remove(@Nullable final T model) {
        entityManager.remove(model);
    }

    @Override
    public void removeAll(@NotNull final Collection<T> models) {
        for (@NotNull final T model : models) remove(model);
    }

    @Override
    public void addAll(@NotNull Collection<T> models) {
        for (@NotNull final T model : models) add(model);
    }

    @Override
    public void set(@NotNull final Collection<T> models) {
        clear();
        models.forEach(this::add);
    }

}