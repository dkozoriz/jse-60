package ru.t1.dkozoriz.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.EmailEmptyException;
import ru.t1.dkozoriz.tm.exception.field.EmailIsExistException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.RoleEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.util.HashUtil;

import javax.persistence.EntityManager;

@Service
@AllArgsConstructor
public class UserDtoService extends AbstractDtoService<UserDto> implements IUserDtoService {

    private final static String NAME = "User";

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    @Getter
    private IUserDtoRepository repository;

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    @Transactional
    public UserDto create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        add(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExist(email)) throw new EmailIsExistException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        add(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDto create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new LoginIsExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDto user = new UserDto();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        add(user);
        return user;
    }

    @Override
    @Nullable
    public UserDto findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return getRepository().findByLogin(login);
    }

    @Override
    @Nullable
    public UserDto findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return getRepository().findByEmail(email);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final UserDto user = findByEmail(email);
        if (user == null) throw new EntityException(getName());
        remove(user);
    }

    @Override
    @NotNull
    @Transactional
    public UserDto setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = findById(id);
        if (user == null) throw new EntityException(getName());
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new PasswordEmptyException();
        user.setPasswordHash(passwordHash);
        update(user);
        return user;
    }

    @Override
    @NotNull
    @Transactional
    public UserDto updateUser(@Nullable final String id,
                              @Nullable final String firstName,
                              @Nullable final String lastName,
                              @Nullable final String middleName
    ) {

        @Nullable final UserDto user = findById(id);
        if (user == null) throw new EntityException(getName());
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        update(user);
        return user;
    }

    @Override
    @NotNull
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @NotNull
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(true);
        update(user);
    }

    @Override
    @Transactional
    public void unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDto user = findByLogin(login);
        if (user == null) throw new EntityException(getName());
        user.setLocked(false);
        update(user);
    }

}