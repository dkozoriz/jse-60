package ru.t1.dkozoriz.tm.service.dto.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IBusinessDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IProjectDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.ITaskDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.dto.model.business.TaskDto;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public class TaskDtoService extends BusinessDtoService<TaskDto> implements ITaskDtoService {

    private final static String NAME = "Task";

    @NotNull
    @Autowired
    private IProjectDtoService projectService;

    @NotNull
    @Autowired
    private ITaskDtoService taskService;

    @NotNull
    @Autowired
    @Getter
    private ITaskDtoRepository repository;

    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    @Transactional
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    @NotNull
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final TaskDto task = new TaskDto();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        task.setProjectId(projectId);
        add(userId, task);
        return task;
    }

    @Override
    @NotNull
    public List<TaskDto> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return getRepository().findAllByProjectId(userId, projectId);
    }

    @NotNull
    public TaskDto bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDto project = projectService.findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final TaskDto task = taskService.findById(userId, taskId);
        if (task == null) throw new EntityException("Task");
        task.setProjectId(projectId);
        taskService.update(userId, task);
        return task;
    }

    @NotNull
    public TaskDto unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final ProjectDto project = projectService.findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final TaskDto task = taskService.findById(userId, taskId);
        if (task == null) throw new EntityException("Task");
        task.setProjectId(null);
        taskService.update(userId, task);
        return task;
    }

}