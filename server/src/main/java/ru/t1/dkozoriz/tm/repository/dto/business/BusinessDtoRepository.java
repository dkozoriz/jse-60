package ru.t1.dkozoriz.tm.repository.dto.business;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IBusinessDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.business.BusinessModelDto;
import ru.t1.dkozoriz.tm.repository.dto.UserOwnedDtoRepository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public abstract class BusinessDtoRepository<T extends BusinessModelDto> extends UserOwnedDtoRepository<T>
        implements IBusinessDtoRepository<T> {

    @Override
    @NotNull
    public List<T> findAllOrderByName(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.name";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<T> findAllOrderByStatus(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.status";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Override
    @NotNull
    public List<T> findAllOrderByCreated(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId ORDER BY m.created";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

}