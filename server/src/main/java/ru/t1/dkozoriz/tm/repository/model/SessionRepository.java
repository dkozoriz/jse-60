package ru.t1.dkozoriz.tm.repository.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.dkozoriz.tm.api.repository.dto.ISessionDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.ISessionRepository;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.model.Session;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends UserOwnedRepository<Session>
        implements ISessionRepository {

    @Override
    @NotNull
    protected Class<Session> getClazz() {
        return Session.class;
    }

}