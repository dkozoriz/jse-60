package ru.t1.dkozoriz.tm.service.model.business;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.model.IBusinessRepository;
import ru.t1.dkozoriz.tm.api.repository.model.ITaskRepository;
import ru.t1.dkozoriz.tm.api.repository.model.IUserRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.api.service.model.business.ITaskService;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.ProjectIdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.TaskIdEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.model.business.Project;
import ru.t1.dkozoriz.tm.model.business.Task;
import ru.t1.dkozoriz.tm.repository.model.business.TaskRepository;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
public class TaskService extends BusinessService<Task> implements ITaskService {

    private final static String NAME = "Task";

    @NotNull
    @Autowired
    @Getter
    private ITaskRepository repository;

    @Autowired
    @NotNull
    private IProjectService projectService;

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        add(userId, task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @Nullable Project project = projectService.findById(userId, projectId);
        if (project == null) throw new EntityException(getName());
        task.setProject(project);
        add(userId, task);
        return task;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return getRepository().findAllByProjectId(userId, projectId);

    }

    @Override
    @NotNull
    @Transactional
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) throw new EntityException("Task");
        task.setProject(project);
        update(userId, task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable final Project project = projectService.findById(userId, projectId);
        if (project == null) throw new EntityException("Project");
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) throw new EntityException("Task");
        task.setProject(null);
        update(userId, task);
        return task;
    }

}