package ru.t1.dkozoriz.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.model.IUserOwnedService;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;
import ru.t1.dkozoriz.tm.model.UserOwnedModel;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public abstract class UserOwnedService<T extends UserOwnedModel> extends AbstractService<T> implements IUserOwnedService<T> {

    @NotNull
    protected abstract IUserOwnedRepository<T> getRepository();

    @Override
    @Nullable
    @Transactional
    public T add(@NotNull final String userId, @Nullable final T model) {
        if (model == null) return null;
        getRepository().add(userId, model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final String userId, @NotNull final T model) {
        getRepository().update(userId, model);
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        getRepository().clear(userId);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        return getRepository().findAll(userId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @Nullable final T model) {
        getRepository().remove(userId, model);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(userId, id);
        if (model == null) return;
        remove(userId, model);
    }

    @Override
    @Transactional
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final T model = findByIndex(userId, index);
        if (model == null) return;
        remove(userId, model);
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return getRepository().findById(userId, id);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize(userId)) throw new IndexIncorrectException();
        return getRepository().findByIndex(userId, index);
    }

    @Override
    public int getSize(@NotNull final String userId) {
        return getRepository().getSize(userId);
    }

}