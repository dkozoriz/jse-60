package ru.t1.dkozoriz.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserOwnedDtoRepository;
import ru.t1.dkozoriz.tm.dto.model.UserOwnedModelDto;

import java.util.Collection;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class UserOwnedDtoRepository<T extends UserOwnedModelDto> extends AbstractDtoRepository<T>
        implements IUserOwnedDtoRepository<T> {

    @Override
    public void add(@NotNull final String userId, @NotNull final T model) {
        model.setUserId(userId);
        entityManager.persist(model);
    }

    @Override
    @NotNull
    public List<T> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).getResultList();
    }

    @Override
    @Nullable
    public T findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId AND m.id = :id";
        return (T) entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    @Nullable
    public T findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, getClazz())
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList()
                .stream().findAny().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, Long.class).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m WHERE m.userId = :userId";
        entityManager.createQuery(jpql).setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final T model) {
        model.setUserId(userId);
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final T model) {
        model.setUserId(userId);
        entityManager.remove(model);
    }

    @Override
    public void removeAll(@NotNull final String userId, @NotNull final Collection<T> models) {
        for (@NotNull final T model : models) remove(userId, model);
    }

    @Override
    public void addAll(@NotNull final String userId, @NotNull Collection<T> models) {
        for (@NotNull final T model : models) add(userId, (model));
    }


}
