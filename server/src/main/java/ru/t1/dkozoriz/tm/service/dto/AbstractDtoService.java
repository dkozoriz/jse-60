package ru.t1.dkozoriz.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.IAbstractDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.IAbstractDtoService;
import ru.t1.dkozoriz.tm.dto.model.AbstractModelDto;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IndexIncorrectException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractDtoService<T extends AbstractModelDto> implements IAbstractDtoService<T> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract String getName();

    @NotNull
    protected abstract IAbstractDtoRepository<T> getRepository();

    @Override
    @Nullable
    @Transactional
    public T add(@Nullable final T model) {
        if (model == null) return null;
        getRepository().add(model);
        return model;
    }

    @Override
    @Transactional
    public void update(@NotNull final T model) {
        getRepository().update(model);
    }

    @Override
    @Transactional
    public void clear() {
       getRepository().clear();
    }

    @Override
    @NotNull
    public List<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    @Transactional
    public void remove(@Nullable final T model) {
       getRepository().remove(getRepository().findById(model.getId()));
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final T model = findById(id);
        if (model == null) return;
        remove(model);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        @Nullable final T model = findByIndex(index);
        if (model == null) return;
        remove(model);
    }

    @Override
    @Nullable
    public T findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        return getRepository().findById(id);
    }

    @Override
    @Nullable
    public T findByIndex(@Nullable final Integer index) {
        if (index == null || index <= 0 || index > getSize()) throw new IndexIncorrectException();
        return getRepository().findByIndex(index);
    }

    @Override
    public long getSize() {
        return getRepository().getSize();
    }

}