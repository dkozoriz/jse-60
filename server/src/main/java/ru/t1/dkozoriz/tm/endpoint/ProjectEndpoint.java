package ru.t1.dkozoriz.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.model.business.IProjectService;
import ru.t1.dkozoriz.tm.dto.request.project.*;
import ru.t1.dkozoriz.tm.dto.response.project.*;
import ru.t1.dkozoriz.tm.enumerated.Sort;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.dto.model.SessionDto;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.model.Session;
import ru.t1.dkozoriz.tm.model.business.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService(endpointInterface = "ru.t1.dkozoriz.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectDtoService projectDtoService;

    @Autowired
    private IProjectService projectService;

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectListResponse projectList(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowListRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Sort sort = request.getSort();
        @Nullable final List<ProjectDto> projectList = projectDtoService.findAll(userId, sort);
        return new ShowProjectListResponse(projectList);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeProjectStatusByIdResponse projectChangeStatusById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDto project = projectDtoService.changeStatusById(userId, projectId, status);
        return new ChangeProjectStatusByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ChangeProjectStatusByIndexResponse projectChangeStatusByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectChangeStatusByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable final ProjectDto project = projectDtoService.changeStatusByIndex(userId, projectIndex, status);
        return new ChangeProjectStatusByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteProjectByIdResponse projectCompleteById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDto project =  projectDtoService.changeStatusById(userId, projectId, Status.COMPLETED);
        return new CompleteProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CompleteProjectByIndexResponse projectCompleteByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCompleteByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDto project = projectDtoService.changeStatusByIndex(userId, projectIndex, Status.COMPLETED);
        return new CompleteProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartProjectByIdResponse projectStartById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDto project = projectDtoService.changeStatusById(userId, projectId, Status.IN_PROGRESS);
        return new StartProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public StartProjectByIndexResponse projectStartByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectStartByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDto project = projectDtoService.changeStatusByIndex(userId, projectIndex, Status.IN_PROGRESS);
        return new StartProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public CreateProjectResponse projectCreate(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectCreateRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDto project = projectDtoService.create(userId, name, description);
        return new CreateProjectResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ListClearProjectResponse projectListClear(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectListClearRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        projectDtoService.clear(userId);
        return new ListClearProjectResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveProjectByIdResponse projectRemoveById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        projectService.removeById(projectId);
        return new RemoveProjectByIdResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public RemoveProjectByIndexResponse projectRemoveByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectRemoveByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        projectService.removeByIndex(projectIndex);
        return new RemoveProjectByIndexResponse();
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectByIdResponse projectShowById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final ProjectDto project = projectDtoService.findById(userId, projectId);
        return new ShowProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public ShowProjectByIndexResponse projectShowByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectShowByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final ProjectDto project = projectDtoService.findByIndex(userId, projectIndex);
        return new ShowProjectByIndexResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateProjectByIdResponse projectUpdateById(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIdRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDto project = projectDtoService.updateById(userId, projectId, name, description);
        return new UpdateProjectByIdResponse(project);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public UpdateProjectByIndexResponse projectUpdateByIndex(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull ProjectUpdateByIndexRequest request
    ) {
        @NotNull final SessionDto session = checkPermission(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer projectIndex = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable final ProjectDto project = projectDtoService.updateByIndex(userId, projectIndex, name, description);
        return new UpdateProjectByIndexResponse(project);
    }

}