package ru.t1.dkozoriz.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IAbstractRepository<T extends AbstractModel> {

    void add(T model);

    void update(T model);

    void clear();

    @Nullable List<T> findAll();

    T findById(@NotNull String id);

    T findByIndex(@NotNull Integer index);

    int getSize();

    void remove(T model);

    void removeAll(@NotNull Collection<T> models);

    void addAll(@NotNull Collection<T> models);

    void set(@NotNull Collection<T> models);
}
