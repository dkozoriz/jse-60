package ru.t1.dkozoriz.tm.service.dto.business;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkozoriz.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.dkozoriz.tm.api.repository.dto.business.IProjectDtoRepository;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends BusinessDtoService<ProjectDto> implements IProjectDtoService {

    private final static String NAME = "Project";

    @NotNull
    @Autowired
    @Getter
    private IProjectDtoRepository repository;

    @NotNull
    public String getName() {
        return NAME;
    }

    @NotNull
    @Transactional
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull final ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

}