package ru.t1.dkozoriz.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.dkozoriz.tm.api.service.IPropertyService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.component.Bootstrap;
import ru.t1.dkozoriz.tm.configuration.ServerConfiguration;
import ru.t1.dkozoriz.tm.dto.model.UserDto;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.entity.EntityException;
import ru.t1.dkozoriz.tm.exception.field.*;
import ru.t1.dkozoriz.tm.exception.user.LoginEmptyException;
import ru.t1.dkozoriz.tm.exception.user.LoginIsExistException;
import ru.t1.dkozoriz.tm.exception.user.PasswordEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.migration.AbstractSchemeTest;
import ru.t1.dkozoriz.tm.service.dto.UserDtoService;
import ru.t1.dkozoriz.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.List;


@Category(UnitCategory.class)
public class UserServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final List<UserDto> userList = new ArrayList<>();

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private static IUserDtoService userService;

    @BeforeClass
    public static void beforeTest() throws LiquibaseException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserDtoService.class);
        propertyService = context.getBean(IPropertyService.class);

        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final UserDto user = new UserDto();
            user.setLogin("user " + i);
            user.setPasswordHash("password " + i);
            user.setEmail("user" + i + "@mail");
            userList.add(user);
            userService.add(user);
        }
    }

    @Test
    public void testAddUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final UserDto user = new UserDto();
        user.setLogin("testUser");
        user.setPasswordHash("pass");
        userService.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
        userService.removeById(user.getId());
    }

    @Test
    public void testClearUser() {
        final int expectedNumberOfEntries = 0;
        userService.clear();
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testFindAllUser() {
        Assert.assertEquals(userList.size(), userService.findAll().size());
    }

    @Test
    public void testRemoveUser() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES;
        @NotNull final UserDto user = new UserDto();
        user.setLogin("testUser");
        user.setPasswordHash("pass");
        userService.add(user);
        userService.remove(user);
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testRemoveUserById() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        @NotNull final UserDto user = userList.get(0);
        userService.removeById(user.getId());
        Assert.assertEquals(expectedNumberOfEntries, userService.getSize());
    }

    @Test
    public void testRemoveUserByIdNegative() {
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> userService.removeById(id));
    }

    @Test
    public void testFindUserById() {
        final String id = userList.get(1).getId();
        Assert.assertNotNull(userService.findById(id));
    }

    @Test
    public void testGetSizeUser() {
        Assert.assertEquals(userList.size(), userService.getSize());
    }

    @Test
    public void testCreate() {
        @NotNull final String login = "login_test";
        @NotNull final String password = "password";
        @NotNull final String email = "email";
        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin(login);
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        user1.setEmail(email);
        @NotNull final UserDto user2 = userService.create(login, password, email);
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getEmail(), user2.getEmail());
    }

    @Test
    public void testCreateNegative() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final String email = "email";
        @Nullable final String login2 = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(login2, password, email));
        @Nullable final String login3 = userList.get(0).getLogin();
        Assert.assertThrows(LoginIsExistException.class, () -> userService.create(login3, password, email));
        @Nullable final String password2 = null;
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, password2, email));
        @Nullable final String email2 = userList.get(0).getEmail();
        Assert.assertThrows(EmailIsExistException.class, () -> userService.create(login, password, email2));
    }

    @Test
    public void testCreateRole() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final Role role = Role.USUAL;
        @NotNull final UserDto user1 = new UserDto();
        user1.setLogin(login);
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        user1.setRole(role);
        @NotNull final UserDto user2 = userService.create(login, password, role);
        Assert.assertEquals(user1.getLogin(), user2.getLogin());
        Assert.assertEquals(user1.getPasswordHash(), user2.getPasswordHash());
        Assert.assertEquals(user1.getRole(), user2.getRole());
    }

    @Test
    public void testCreateRoleNegative() {
        @NotNull final String login = "login";
        @NotNull final String password = "password";
        @NotNull final Role role = Role.USUAL;
        @Nullable final String login2 = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.create(login2, password, role));
        @Nullable final String login3 = userList.get(0).getLogin();
        Assert.assertThrows(LoginIsExistException.class, () -> userService.create(login3, password, role));
        @Nullable final String password2 = null;
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.create(login, password2, role));
        @Nullable final Role role2 = null;
        Assert.assertThrows(RoleEmptyException.class, () -> userService.create(login, password, role2));
    }

    @Test
    public void testIsLoginExist() {
        @NotNull final String login = userList.get(0).getLogin();
        @NotNull final Boolean loginExist1 = userList.stream().anyMatch(u -> login.equals(u.getLogin()));
        @NotNull final Boolean loginExist2 = userService.isLoginExist(login);
        Assert.assertEquals(loginExist1, loginExist2);
    }

    @Test
    public void testIsEmailExist() {
        @NotNull final String email = userList.get(0).getEmail();
        @NotNull final Boolean emailExist1 = userList.stream().anyMatch(u -> email.equals(u.getEmail()));
        @NotNull final Boolean emailExist2 = userService.isEmailExist(email);
        Assert.assertEquals(emailExist1, emailExist2);
    }

    @Test
    public void testFindUserByLogin() {
        final String login = userList.get(1).getLogin();
        Assert.assertNotNull(userService.findByLogin(login));
    }

    @Test
    public void testFindUserByEmail() {
        final String email = userList.get(1).getEmail();
        Assert.assertNotNull(userService.findByEmail(email));
    }

    @Test
    public void testFindUserByEmailNegative() {
        @Nullable final String email = null;
        Assert.assertThrows(EmailEmptyException.class, () -> userService.findByEmail(email));
    }

    @Test
    public void testSetPassword() {
        @NotNull final UserDto user1 = userList.get(0);
        @NotNull final String password = "password";
        user1.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final UserDto user2 = userList.get(0);
        Assert.assertEquals(user1.getPasswordHash(), userService.setPassword(user2.getId(), password).getPasswordHash());
    }

    @Test
    public void testSetPasswordNegative() {
        @NotNull final UserDto user1 = userList.get(0);
        @NotNull final String password = "password";
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> userService.setPassword(id, password));
        Assert.assertThrows(PasswordEmptyException.class, () -> userService.setPassword(user1.getId(), null));
    }

    @Test
    public void testUpdateUser() {
        @NotNull final String userId = userList.get(0).getId();
        @NotNull final String name = "test";
        userList.get(0).setFirstName(name);
        userList.get(0).setMiddleName(name);
        userList.get(0).setLastName(name);
        @NotNull UserDto user = userService.updateUser(userId, name, name, name);
        Assert.assertEquals(userList.get(0).getFirstName(), user.getFirstName());
        Assert.assertEquals(userList.get(0).getMiddleName(), user.getMiddleName());
        Assert.assertEquals(userList.get(0).getLastName(), user.getLastName());
    }

    @Test
    public void testLockUserByLogin() {
        @NotNull final UserDto user = userList.get(0);
        @NotNull final String login = user.getLogin();
        user.setLocked(true);
        userService.lockUserByLogin(login);
        Assert.assertEquals(user.getLocked(), userService.findByLogin(login).getLocked());
    }

    @Test
    public void testLockUserByLoginNegative() {
        @Nullable final String login = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.lockUserByLogin(login));
    }

    @Test
    public void testUnLockUserByLogin() {
        @NotNull final UserDto user = userList.get(0);
        @NotNull final String login = user.getLogin();
        user.setLocked(false);
        userService.unLockUserByLogin(login);
        Assert.assertEquals(user.getLocked(), userService.findByLogin(login).getLocked());
    }

    @Test
    public void testUnLockUserByLoginNegative() {
        @Nullable final String login = null;
        Assert.assertThrows(LoginEmptyException.class, () -> userService.unLockUserByLogin(login));
    }

    @After
    public void clearData() {
        userList.clear();
        userService.clear();
    }

}