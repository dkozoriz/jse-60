package ru.t1.dkozoriz.tm.service;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import liquibase.servicelocator.ServiceLocator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.dkozoriz.tm.api.service.IConnectionService;
import ru.t1.dkozoriz.tm.api.service.IServiceLocator;
import ru.t1.dkozoriz.tm.api.service.dto.IUserDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.IProjectDtoService;
import ru.t1.dkozoriz.tm.api.service.dto.business.ITaskDtoService;
import ru.t1.dkozoriz.tm.component.Bootstrap;
import ru.t1.dkozoriz.tm.configuration.ServerConfiguration;
import ru.t1.dkozoriz.tm.dto.model.business.ProjectDto;
import ru.t1.dkozoriz.tm.enumerated.Role;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.exception.field.DescriptionEmptyException;
import ru.t1.dkozoriz.tm.exception.field.IdEmptyException;
import ru.t1.dkozoriz.tm.exception.field.NameEmptyException;
import ru.t1.dkozoriz.tm.exception.field.StatusEmptyException;
import ru.t1.dkozoriz.tm.exception.user.UserIdEmptyException;
import ru.t1.dkozoriz.tm.marker.UnitCategory;
import ru.t1.dkozoriz.tm.migration.AbstractSchemeTest;
import ru.t1.dkozoriz.tm.service.dto.UserDtoService;
import ru.t1.dkozoriz.tm.service.dto.business.ProjectDtoService;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;


@Category(UnitCategory.class)
public class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static String userId1 = "";

    @NotNull
    private static String userId2 = "";

    @NotNull
    private final List<ProjectDto> projectList = new ArrayList<>();

    @NotNull
    private static IProjectDtoService projectService;

    @NotNull
    private static IUserDtoService userService;

    @BeforeClass
    public static void beforeTest() throws LiquibaseException {
        ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        userService = context.getBean(IUserDtoService.class);
        projectService = context.getBean(IProjectDtoService.class);

        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
        userId1 = userService.create("user1", "pass", Role.USUAL).getId();
        userId2 = userService.create("user2", "pass", Role.USUAL).getId();
    }

    @Before
    public void initTest() {
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final ProjectDto project = new ProjectDto("Project" + i, "Description" + i);
            if (i <= 10 / 2) project.setUserId(userId1);
            else project.setUserId(userId2);
            projectService.add(project);
            projectList.add(project);
        }
    }

    @Test
    public void testAddProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final ProjectDto project = new ProjectDto();
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testAddUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES + 1;
        @NotNull final ProjectDto project = new ProjectDto();
        projectService.add(userId1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testUpdateProject() {
        @NotNull final ProjectDto project = projectList.get(0);
        project.setName("projectUpdate");
        projectService.update(project);
        Assert.assertEquals(projectService.findById(project.getId()).getName(), "projectUpdate");
    }

    @Test
    public void testUpdateUserOwnedProject() {
        final int expectedNumberOfEntries = 0;
        projectService.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearProject() {
        final long expectedNumberOfEntries = 0;
        projectService.clear();
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testClearUserOwnedProject() {
        final long expectedNumberOfEntries = projectService.getSize(userId2);
        projectService.clear(userId1);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testFindAllProject() {
        final List<ProjectDto> findAllProjectList = projectService.findAll();
        Assert.assertEquals(findAllProjectList.size(), projectService.getSize());
    }

    @Test
    public void testFindAllUserOwnedProject() {
        final List<ProjectDto> findAllProjectList = projectService.findAll(userId1);
        Assert.assertEquals(findAllProjectList.size(), projectService.getSize(userId1));
    }

    @Test
    public void testFindByIdProject() {
        final String projectId = projectList.get(0).getId();
        final ProjectDto project = projectService.findById(projectId);
        Assert.assertNotNull(project);
    }

    @Test
    public void testFindByIdUserOwnedProject() {
        final String projectId = projectList.get(0).getId();
        final ProjectDto project = projectService.findById(userId1, projectId);
        Assert.assertNotNull(project);
    }

    @Test
    public void testRemoveProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String projectId = projectList.get(0).getId();
        final ProjectDto project = projectService.findById(projectId);
        projectService.remove(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String projectId = projectList.get(0).getId();
        final ProjectDto project = projectService.findById(userId1, projectId);
        projectService.remove(userId1, project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveByIdProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String projectId = projectList.get(0).getId();
        projectService.removeById(projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testRemoveByIdUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES - 1;
        final String projectId = projectList.get(0).getId();
        projectService.removeById(userId1, projectId);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize());
    }

    @Test
    public void testGetSizeProject() {
        Assert.assertEquals(projectList.size(), projectService.getSize());
    }

    @Test
    public void testGetSizeUserOwnedProject() {
        final int expectedNumberOfEntries = NUMBER_OF_ENTRIES / 2;
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(userId1));
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(userId2));
    }

    @Test
    public void testChangeStatusById() {
        @Nullable final ProjectDto project = projectService
                .changeStatusById(userId1, projectList.get(0).getId(), Status.IN_PROGRESS);
        @Nullable final Status status = project.getStatus();
        Assert.assertEquals(Status.IN_PROGRESS, status);
    }

    @Test
    public void testChangeStatusByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .changeStatusById(userId, projectId, Status.IN_PROGRESS));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService
                .changeStatusById(userId1, id, Status.IN_PROGRESS));
        @Nullable final Status status = null;
        Assert.assertThrows(StatusEmptyException.class, () -> projectService
                .changeStatusById(userId1, projectId, status));
    }

    @Test
    public void testUpdateById() {
        @Nullable final ProjectDto project = projectList.get(0);
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertEquals(name, projectService.updateById(userId1, project.getId(), name, description).getName());
        Assert.assertEquals(description, projectService.updateById(userId1, project.getId(), name, description).getDescription());
    }

    @Test
    public void testUpdateByIdNegative() {
        @Nullable final String userId = null;
        @Nullable final String projectId = projectList.get(0).getId();
        @Nullable final String name = "new name";
        @Nullable final String description = "new description";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .updateById(userId, projectId, name, description));
        @Nullable final String id = null;
        Assert.assertThrows(IdEmptyException.class, () -> projectService
                .updateById(userId1, id, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> projectService
                .updateById(userId1, projectId, name2, description));
    }

    @Test
    public void testCreateProject() {
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        @Nullable final ProjectDto project = new ProjectDto(name);
        project.setDescription(description);
        Assert.assertEquals(project.getName(), projectService.create(userId1, name, description).getName());
        Assert.assertEquals(project.getDescription(), projectService.create(userId1, name, description).getDescription());
    }

    @Test
    public void testCreateProjectNegative() {
        @Nullable final String userId = null;
        @Nullable final String name = "name";
        @Nullable final String description = "description";
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService
                .create(userId, name, description));
        @Nullable final String name2 = null;
        Assert.assertThrows(NameEmptyException.class, () -> projectService
                .create(userId1, name2, description));
        @Nullable final String description2 = null;
        Assert.assertThrows(DescriptionEmptyException.class, () -> projectService
                .create(userId1, name, description2));
    }


    @After
    public void clearData() {
        projectList.clear();
        projectService.clear();
    }

}